package view;

import integration.*;
import controller.*;

/*
 * Handling user interactions and passes it to the controller
 * as well as printing messages with transaction information.
 */
public class View {

	private final Controller controller;
	
	/**
	 * Constructor
	 * @param controller 	Controller for the program.
	 */
	public View(Controller controller) {
		this.controller = controller;
	}
	
	/**
	 * Hard coded interaction flow for processing a sale.
	 * @return
	 */
	public boolean flow(){
			
		controller.initSale();
		
		enterItem(1, 1);
		enterItem(2, 1);
		enterItem(3, 1);
		enterItem(4, 1);
		enterItem(100, 1);
		
		endSale();
		
		pay(100);
		
		return false;
	}
	
	/**
	 * View function for entering an item and handling the data
	 * returned from the controller and prints it.
	 * @param itemIdentifier
	 * @param quantity
	 */
	public void enterItem(int itemIdentifier, int quantity) {
		
		ItemDTO item = controller.addItemToSale(itemIdentifier, quantity);
		
		if(item != null) {
			System.out.println(quantity + " x " + item.getName() + " was added to sale."); 
		}
		else {
			System.out.println("Item not found."); 
		}
	}
	
	/**
	 * View function for ending the sale and handling data
	 * returned by the controller and prints it.
	 */
	public void endSale() {
		
		double totals = controller.endSale();
		System.out.println("Totals with taxes: " + totals); 
	}
	
	/**
	 * View function for pay, handling return data
	 * from the controller and prints it.
	 * @param amount
	 */
	public void pay(double amount) {
		
		double change = controller.pay(amount);
		
		if(change == -1) {
			System.out.println("Paid amount is to low."); 
		}
		else {
			System.out.println("Payment ok.");
		}
	}
}
