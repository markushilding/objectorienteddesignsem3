package model;

import static org.junit.Assert.*;
import org.junit.Test;

import integration.*;

/*
 * Testing methods in Sale class.
 */
public class SaleTest {

	
	@Test
	public void testAddItem() {
		
		Printer printer = new Printer();
		Sale sale = new Sale(printer);
		ItemDTO item = new ItemDTO(1, "Banana", 8.99, 0.12);
		
		assertEquals(item.getName(), sale.addItem(1, 1).getName());
		assertEquals(null, sale.addItem(-55, 1));
	}
	
	
	@Test
	public void testEndSale() {
		Printer printer = new Printer();
		Sale sale = new Sale(printer);

		sale.addItem(9, 1);

		assertEquals(62.5, sale.endSale(), 0);
	}
	
	
	@Test
	public void testPay(){
		
		Printer printer = new Printer();
		Sale sale = new Sale(printer);
		sale.addItem(9, 1);
		
		assertEquals(7.5, sale.pay(70), 0);
		assertEquals(-1, sale.pay(30), 0);
	}
}
