package model;

import integration.ItemDTO;

/*
 * Class handling payment logic such as
 * calculating totals with taxes and
 * how much change to give to the customer.
 */
public class Payment {
	
	private double totalsWithVAT = 0;
	
	/**
	 * Returns the cost for all items, including taxes.
	 * @return total cost for the sale.
	 */
	public double getTotals() {
		return totalsWithVAT;
	}
	
	public void addItem(ItemDTO item, int quantity) {
		
		double price = item.getPrice();
		double taxes = item.getPrice() * item.getTaxRate();
			
		double totals = (price + taxes)*quantity;
		
		totalsWithVAT += totals; 
	}
	
	/**
	 * Calculates the change the customer should be handed back.
	 * @param amount
	 * @return Amount of change.
	 */
	public double getChange(double amount) {
		return amount - totalsWithVAT;
	}
}
