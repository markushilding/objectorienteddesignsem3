package model;

/*
 * Class that is building receipts from sales data.
 */
public class Receipt {

	private String receipt;	
	
	/**
	 * Creates a receipt with info in salesDTO
	 * @param info
	 */
	public Receipt(SaleDTO info) {
		
		StringBuilder rcpt = new StringBuilder();
		
		rcpt.append("---------------------------------------\n");
		rcpt.append("RECEIPT\n" + info.itemsToString() + "\n");
		rcpt.append("---------------------------------------\n");
		
		receipt = rcpt.toString();
	}
	
	/**
	 * Returns receipt
	 * @return receipt
	 */
	public String getReceipt() {
		return receipt;
	}
}
