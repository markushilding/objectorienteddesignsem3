package startup;

import controller.Controller;
import view.View;
import integration.Printer;

/*
 * main executable
 */
public class Main {

	public static void main(String[] args) {
			
		Printer printer = new Printer();
		Controller controller = new Controller(printer);
		View view = new View(controller);
		
		while(view.flow());
		
		System.exit(0);
	}
}